public class GameManager{
	
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	private GameManager(Deck drawPile, Card centerCard, Card playerCard){
		this.drawPile = drawPile;
		this.centerCard = centerCard;	
		this.playerCard = playerCard;
	}
	
	public GameManager(){
		this(new Deck(), null, null);
		
		this.dealCards();
	}
	
	public String toString(){
		String s = "--------------------------------------------------\n";
		s += "Center card: ";
		s += this.centerCard.toString() + "\n";
		s += "Player card: ";
		s += this.playerCard.toString() + "\n";
		s += "--------------------------------------------------\n";
		return s;
	}
	
	public void dealCards(){
		this.drawPile.shuffle();
		
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	//refactored getter
	public int getNumberOfCards()
	{
		return this.drawPile.length();
	}
	
	public int calculatePoints(){
		final int SAME_VALUE_POINTS = 4;
		final int SAME_SUIT_POINTS = 2;
		final int NEITHER_POINTS = -1;
		
		if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return SAME_VALUE_POINTS;
		}
		
		if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return SAME_SUIT_POINTS;
		}
		
		return NEITHER_POINTS;
	}
}
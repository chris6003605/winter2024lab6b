import java.util.Random;
public class Deck{
	private final int MAX_NUM_CARDS = 52;
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck()
	{
	super();
	
	//A array of string that holds value of all the card numbers
	final String[] VALUES = { "Ace", "Two",  "Three", "Four", "Five", "Six", "Seven", "Eight",  "Nine", "Ten", "Jack", "Queen", "King"};
	final String[] SUITS = {"Hearts", "Diamonds", "Spades", "Clubs"};
	
	this.numberOfCards = MAX_NUM_CARDS;
	this.cards = new Card[this.numberOfCards];	
	this.rng = new Random();
	
	int currentCard = 0;
	
	//for each suit (String) in the array SUITS
	for(String suit : SUITS)
		{
		for(String value : VALUES)
			{
			this.cards[currentCard] = new Card(suit, value);
			currentCard++;
			}
		}
	}
	public int length(){
		return numberOfCards;
	}
	public Card drawTopCard(){
		Card c = cards[--numberOfCards];
		this.cards[numberOfCards] =  null;
		return c;
	}
	
	public String toString(){
		String str = "";
			
		for(int i = 0; i < this.numberOfCards; i++){
			str += cards[i].toString() +"\n";
		}
			
		return str;
	}
	
	public void shuffle(){
		Card tempCard = null;
		int swapIndex = 0;
		for(int i = 0; i < numberOfCards; i++){
			swapIndex = this.rng.nextInt(numberOfCards);
			tempCard = this.cards[i];
			this.cards[i] = this.cards[swapIndex];
			this.cards[swapIndex] = tempCard;
		}
		
	}
	//TODO creating all the cards should be a method.
}
import java.util.Scanner;
public class LuckyCardGameApp{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 1;
		String result = "loses";
		System.out.println("Welcome to Lucky! Card game!");
		
		while(manager.getNumberOfCards() > 1 && totalPoints < 5){
			System.out.println("Round: " + round);
			System.out.print(manager);
			
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round " + round + ": " + totalPoints);
			System.out.println("*****************************************************");
			
			round++;
			manager.dealCards();
		}
		
		if(totalPoints >= 5){
			result = "wins";
		}
		System.out.print("Player " + result + " with: " + totalPoints + " points");
	}
}